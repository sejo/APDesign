var logo = function(p){

var font;
var lm;

var alphaOn, alphaOff;

p.preload = function(){

	font = p.loadFont("data/PxGrotesk-Bold.ttf");


};

p.setup = function(){
	p.createCanvas(1024,650);


	alphaOn = 255;
	alphaOff = 20;

	lm = new LetterManager();

	lm.setTargetPositions("CODE");


	//

};


p.draw = function(){
	p.background(255);
	p.fill(0);
	p.noStroke();
	lm.draw(p.width/4,p.height/2);
	lm.update();
	
};

function LetterManager(text){
	this.letters = [];
	this.separationFactor = 1.05;
	this.t = 0;

	this.timerWait = 60;
	this.timer = this.timerWait;
	this.counter = 0;
	this.counterTh = 4;
	this.tSize = 60;
	this.started = false;

	this.mainString = "ADJACENT POSSIBLE";
	this.Strings = ["CODE","SLEEP","LANDSCAPE","ACID","ACTION","ADAPT","JOIN","CAPABLE","SPEED","SANDCASTLE","SCANDAL","CAPITAL","SPACE"];

	this.targetIndex = 0;

	var text = this.mainString;
	var letter;
	var x = 0;
	for(var i=0; i<text.length; i++){
		letter = new Letter(text.charAt(i),x,0);
		letter.tSize = this.tSize;
		letter.font = font;

		x += letter.getWidth()*this.separationFactor;

		this.letters.push(letter);
	}
	console.log(this.letters);

	this.draw = function(x,y){
		p.push();
		p.translate(x,y);
		for(i in this.letters){
			this.letters[i].draw();
		}
		p.pop();
	}

	this.update = function(){
		for(i in this.letters){
			this.letters[i].interpolate(this.t);
		}
		if(this.t<1){
			if(this.timer>0){
				this.timer--;
			}
			else{
				this.t+=0.01;
			}
		}
		else{
			var ind;
			this.counter ++;

			for(i in this.letters){
				this.letters[i].setOriginals();
			}

			if(this.counter>=this.counterTh){
				this.setTargetPositions(this.mainString);
				this.counter = 0;
				this.started = true;
			}
			else{
				if(this.started && this.counter==1){
					this.timer = this.timerWait;
				}
				do{
					ind = p.floor(p.random(this.Strings.length));
				}while(ind==this.targetIndex);
				this.targetIndex = ind;
				this.setTargetPositions(this.Strings[ind]);
			}

			this.t = 0;
		}

	}

	this.setTargetPositions = function(s){
		var c;
		var index;
		var letter;
		var x = 0;

		// Reset letters
		for(i in this.letters){
			this.letters[i].isChosen = false;
			this.letters[i].targetAlpha = alphaOff;
		}

		for(var i=0; i<s.length; i++){
			c = s.charAt(i);
			do{
				index = p.floor(p.random(this.letters.length));
				letter = this.letters[index];
				//console.log(letter.character + " " + letter.isChosen);
			}while(letter.isChosen || letter.character!=c);
			this.letters[index].isChosen = true;
			this.letters[index].targetPos = p.createVector(x,0);
			this.letters[index].targetAlpha = alphaOn;
			x += this.letters[index].getWidth()*this.separationFactor;
		}

	}
}


function Letter(c,x,y){
	this.state;
	this.originalAlpha = alphaOn;
	this.currAlpha = alphaOn;
	this.targetAlpha = alphaOn;

	this.character = c;

	this.originalPos = p.createVector(x,y);
	this.currPos = p.createVector(x,y);
	this.targetPos = p.createVector(x,y);

	this.tSize = 10;
	this.font;

	this.isChosen = false;

	this.interpolate = function(t){
	//	this.currPos = p5.Vector.lerp(this.originalPos,this.targetPos,t);

		//this.currPos.x = p.lerp(this.originalPos.x,this.targetPos.x,t);
		//this.currPos.y = p.lerp(this.originalPos.y,this.targetPos.y,t);
		//this.currAlpha = p.lerp(this.originalAlpha,this.targetAlpha,t);

		this.currPos.x = this.cosInter(this.originalPos.x,this.targetPos.x,t);
		this.currPos.y = this.cosInter(this.originalPos.y,this.targetPos.y,t);
		this.currAlpha = this.cosInter(this.originalAlpha,this.targetAlpha,t);
	}

	this.cosInter = function(a,b,t){
		return a + (b-a)*this.cosApp(t);
	}

	this.cosApp = function(t){
		var t2 = t*t;
		var t4 = t2*t2;
		var t6 = t4*t2;

		return (4.0/9)*t6 - (17.0/9)*t4 + (22/9.0)*t2;
	}


	this.setOriginals = function(){
		this.originalPos.set(this.targetPos);
		this.originalAlpha = this.targetAlpha;
	}

	this.draw = function(){
		var x = this.currPos.x;
		var y = this.currPos.y;
		p.fill(20,this.currAlpha);
		p.textSize(this.tSize);
		p.textFont(this.font);
		p.text(this.character,x,y);
	}

	this.getWidth = function(){
		p.textSize(this.tSize);
		p.textFont(this.font);
		return p.textWidth(this.character);
	}
};

};

var logoSketch = new p5(logo);

