var texts = [];
var titles = [ "Introduction",
		"The New Infrastructure of Inequality",
		"What's With The Mirrors, Danny Rozin?",
		"Make Virtual Reality Great Again",
		"Data Dumped",
		"Coding === NewLiteraryForm;",
		"Othernet, an Internet Island",
		"Welcome to the Fandom Singularity (An Excerpt)",
		"When Siri Met My Son: UX Lessons From My Baby",
		"Design and the Ideology of Things",
		"The Prince of Humbug and the King of Debt",
		"Reflections on my 100 Days of Making",
		"As We (Still) May Think",
		"The Possible in Adjacent"
];
var bgcolor;
var edges;
var nodesWords;
var universe;
var univ =[];
var nodes = [];
var N = 14;
var minLinks;
var maxLinks;
var t;
var colorFocus; 
var posPaths;
var freeRect;
var titleFont;
var imgs = [];
function preload(){
	edges = loadJSON("data/links.json");
	nodesWords = loadJSON("data/nodeWords.json");
	universe = loadJSON("data/universe.json");
	titleFont = loadFont("data/PxGrotesk-Bold.ttf");
	for(var i=0; i<N; i++){
		var str = "" + i;
		var pad = "00";
		var ans = pad.substring(0, pad.length - str.length) + str;
		imgs[i] = loadImage("data/"+ans+".png");
	}
}
function setup(){
	createCanvas(1024,650);
	bgcolor = color(249, 244, 239);
	for(var i=0; i<N; i++){
		texts[i] = i;
	}
	freeRect = {	x: width*0.25,
			y: height*0.02,
			w: width*0.5,
			h: height*0.15,
			midx: 0,
			midy: 0,
			endx: 0,
			endy: 0
	};
	freeRect.midx = freeRect.x + freeRect.w/2;
	freeRect.midy = freeRect.y + freeRect.h/2;
	freeRect.endx = freeRect.x + freeRect.w;
	freeRect.endy = freeRect.y + freeRect.h;
	background(255);
	colorFocus = color(255,185,0,230);
	for(i in edges){
		edges[i].focus = false;
	}
	console.log(edges[3]);
	for(i in universe){
		/*
		univ.push({word:universe[i],
				focusMouse:false,
				focusNode:false,
				vec:createVector(random(width),random(height))
		});
		*/
		univ[universe[i]] = {word:universe[i],
				focusMouse:false,
				focusNode:false,
				vec:createVector(random(width),random(height))};
	}
	univ.sort( function(a,b){ 
		var wa = a.word;
		var wb = b.word;
		if(wa<wb){
			return -1;
		}
		if(wa>wb){
			return 1;
		}
		return 0;
		});
	console.log(univ);
	// univ sort?
	var x,y;
	var r = 30; // radius of original arrangement
	var d=20; // diameter of node
	var node = [];
	var a = 0;
	var a2,r2;
	for(var i=0; i<N; i++){
//		a2 = a + random(-PI/8,PI/8);
		a2 = a + random(0,2*PI);
/*
		r2 = r + random(-1,1)*r*0.01;
		*/
		r2 = r;
		x = r2*cos(a2) + width/2; 
		y = r2*sin(a2) + height/2;
		node =[];
		node.pos = createVector(x,y);
		node.originalpos = createVector(x,y);
		node.index = i;
		node.name= texts[i];
		node.title = titles[i];
		node.focus = false;
		node.words = nodesWords[i];
		a+= TWO_PI/(N+1);
		nodes.push(node);
	}
	t = 0;
	calculateMinAndMaxLinkWordsN();
	for(var i=0; i<200; i++){
		iterateSimForces();
	}
	posPaths = [];
	var x,y,nv;
	var margin = 50;
	for(var i in univ){
		do{
			x = random(margin,width-margin);
			y = random(margin,height-margin);
			nv = createVector(x,y);
		}while(isColliding(nv,50) || isInRect(nv.x,nv.y,freeRect.x,freeRect.y,freeRect.w,freeRect.h));
//		}while(isColliding(nv,50));
		univ[i].vec.set(nv);
	}
}
function draw(){
	background(bgcolor);
	var title = "Explore...";
	rectMode(CORNER);
	noFill();
//	rect(freeRect.x,freeRect.y,freeRect.w,freeRect.h);
	var d = 80;
	var tSize = 11;
	var v1,v2
	textAlign(LEFT);
	textSize(tSize);
	// DRAW EDGES
	for(var i=0; i<N; i++){
		v1 = nodes[i].pos;
		//
		// Draw edges
		for(var j=i; j<N; j++){
			var edge = getEdge(i,j);
			v2 = nodes[j].pos;
			var ne = getEdgeLength(i,j);
//			console.log(ne);
			var th = 2;
			if(edge.focus){
				strokeWeight(2);
				stroke(colorFocus,255);
				th = 4;
			}
			else{
				strokeWeight(1);
				stroke(100,100);
				th = 2;
			}
//			stroke(100,200);
			var wu;
			var st;
			for(var z=0; z<ne; z++){
				wu = univ[edge.words[z]].vec;
				st = univ[edge.words[z]].focusNode;
				// connect to node
//				drawRandomLine(v1,v2,z+i+j,th);
				
				// If only node is focused,
				// don't highlight the second part
				if(st){
					if(nodes[i].focus){
						drawRandomLine(v1,wu,z+i+j,th);
					}
					else{
						drawRandomLine(wu,v2,z+i+j,th);
					}
					strokeWeight(1);
					stroke(100,255);
					th = 2;
					if(nodes[i].focus){
						drawRandomLine(wu,v2,z+i+j,th);
					}
					else{
						drawRandomLine(v1,wu,z+i+j,th);
					}
				}
				else{
					// conect to word and node
					drawRandomLine(v1,wu,z+i+j,th);
					drawRandomLine(wu,v2,z+i+j,th);
				}
			}
		}
	}
	// DRAW NODES
	for(var i=0; i<N; i++){
		rectMode(CENTER);
		v1 = nodes[i].pos;
		// Draw node
		strokeWeight(1);
		stroke(100,255);
		// Focus node
		if(isMouseWithin(v1.x-d/2,v1.y-d/2,d,d)){
			unfocusEverything();
			focusWordsWithNode(nodes[i]);
			nodes[i].focus = true;
			title = nodes[i].title;
			for(var j=0; j<nodes[i].words.length; j++){
				focusEdgesWithWord(nodes[i].words[j]);
//				console.log(edges);
			}
		}
		if(nodes[i].focus){

			noStroke();
			fill(bgcolor);
		rect(v1.x,v1.y,d,d);
		stroke(colorFocus,255);
		strokeWeight(4);
			tint(255,50);
		image(imgs[i],v1.x-d/2,v1.y-d/2,d,d);
		noFill();
		}
		else{
		strokeWeight(1);
		stroke(100,255);
//		fill(255,255);
			fill(bgcolor);
		}
		rect(v1.x,v1.y,d,d);
	}
	// Words
	var ds = d*0.4;
	var s;
	for(var i in univ){
		rectMode(CENTER);
		fill(bgcolor);
		v1 = univ[i].vec;
		s = univ[i].word;
		if(isMouseWithin(v1.x-ds/2,v1.y-ds/2,ds,ds)){
			unfocusEverything();
			univ[i].focusMouse = true;
			focusWithWord(s);
			title = s;
		}
		else{
			univ[i].focusMouse = false;
		}
		if(univ[i].focusMouse || univ[i].focusNode){
			strokeWeight(2);
			stroke(colorFocus,255);
		}
		else{
			strokeWeight(1);
			stroke(0,150);
		}
		rect(v1.x,v1.y,ds,ds);
		if(univ[i].focusMouse || univ[i].focusNode){
			strokeWeight(1);
			stroke(colorFocus,255);
			fill(0);
//			fill(colorFocus,255);
		}
		else{
		noStroke();
			fill(50,150);
		}
		textStyle(NORMAL);
		textSize(tSize*0.8);
		rectMode(CORNER);
		var tw = textWidth(s);
//		text(nodes[i].title,v1.x+d*0.5,v1.y,tSize*10,tSize*20);
		text(s,v1.x-(tw)*0.5,v1.y-ds*0.15,ds-0.15,tSize*20);
	}
	for(var i=0; i<N; i++){
		v1 = nodes[i].pos;
		// Text
		if(nodes[i].focus){
			strokeWeight(1);
			stroke(colorFocus);
			fill(0);
		
		}else{
		noStroke();
		fill(0);
		}
		textStyle(BOLD);
		textSize(tSize);
		rectMode(CORNER);
//		text(nodes[i].title,v1.x+d*0.5,v1.y,tSize*10,tSize*20);
		text(nodes[i].title,v1.x-d*0.45,v1.y-d*0.45,d-0.15,tSize*20);
	}
//	iterateSimForces();
	t+=0.001;
//	console.log(posPaths);
	//
	/*TEST PATH
	noFill();
	var path = [];
	path.push(createVector(10,10));
	path.push(createVector(10,20));
	path.push(createVector(10,60));
	path.push(createVector(10,80));
	path.push(createVector(50,80));
	path.push(createVector(50,60));
	path.push(createVector(50,50));
	path.push(createVector(100,50));
	path.push(createVector(100,100));
	path.push(createVector(150,100));
	path.push(createVector(150,200));
	path.push(createVector(100,200));
	stroke(0,255,0);
//	fill(255,255,0);
	drawThickPath(path,10);
	stroke(255,0,0);
	beginShape();
	var ver;
	for(var i=0; i<path.length;i++){
		ver = path[i];
		vertex(ver.x,ver.y);
	}
	endShape();
	*/
	rectMode(CORNER);
	var titleSize = 45;
	var tw;
	textFont(titleFont);
	do{
	textSize(titleSize);
	tw= textWidth(title);
	titleSize--;
	}while(tw>freeRect.w*0.9);
	fill(0);
	noStroke();
	text(title,freeRect.x+(freeRect.w-tw)/2,freeRect.midy + titleSize*0.25);
}
function iterateSimForces(){
	var d = 25;
	// Update positions
	var addition;
	var f = 0.04;
	var attraction;
	var repulsion;
	var n1,n2;
	var repfactor = 5000;
	var attfactor = 1;
	for(var i=0; i<N; i++){
		attraction = createVector(0,0);
		repulsion = createVector(0,0);
		n1 = nodes[i].pos;
		for(var j=0; j<N; j++){
			if(i!=j){
				n2 =  nodes[j].pos;
				var dif = p5.Vector.sub(n2,n1);
				var m = dif.mag();
				var rep = createVector(0,0);
				var w = getEdgeNormWeight(i,j);
				dif.normalize();
				rep.set(dif);
				// Repulsion is inversely proportional
				// To distance
				// And to weight?
//				rep.mult(-1/(m*m*(w+0.01)));
				rep.mult(-1/(m*m+w));
				rep.mult(repfactor);
				repulsion.add(rep);
			 	// Attraction is proportional to sq(weight)
				dif.mult(w*w);
				dif.mult(attfactor);
				attraction.add(dif);
			}
		}
		nodes[i].pos.add(attraction);
		nodes[i].pos.add(repulsion);
		// Add continuous movement
		var keepMoving = createVector(0,0);
		keepMoving.set(nodes[i].pos);
		keepMoving.sub(width/2,height/2);
		keepMoving.normalize();
		keepMoving.rotate(HALF_PI);
		keepMoving.mult(0.3);
		nodes[i].pos.add(keepMoving);
		// Contrain positions
		var margin  = d*4;
		var fr = 1.5;
		var fl = 0.5;
		var ft = 0.5;
		var fb = 0.5;
		var nx = nodes[i].pos.x;
		var ny = nodes[i].pos.y;
		if(nx>freeRect.x && nx<freeRect.midx && ny>freeRect.y && ny<freeRect.endy){
			nodes[i].pos.x = freeRect.x-2*d;
		}
		if(ny>freeRect.y && ny<freeRect.midy && nx>freeRect.x && nx<freeRect.endx){
			nodes[i].pos.y = freeRect.y-2*d;
		}
		if(nx>freeRect.midx && nx<freeRect.endx && ny>freeRect.y && ny<freeRect.endy){
			nodes[i].pos.x = freeRect.endx+2*d;
		}
		if(ny>freeRect.midy && ny<freeRect.endy && nx>freeRect.x && nx<freeRect.endx){
			nodes[i].pos.y = freeRect.endy+2*d;
		}
		nodes[i].pos.x = constrain(nodes[i].pos.x,margin*fr,width-margin*fl);
		nodes[i].pos.y = constrain(nodes[i].pos.y,margin*ft,height-margin*fb);
	}
}
function drawRandomLine(p1,p2,rseed,th){
	noFill();
//	randomSeed(rseed);
	var index = ""+int(p1.x)+int(p1.y)+int(p2.x)+int(p2.y)+rseed;
	var stepsize = 12;
	var collisionsize = stepsize;
	var probchangedir = 0.05; // probability of changing direction
	var dif, difx, dify;
	var pos;
	var dirx;
	var done;
	var collisionCount;
	var topCollisionCount = 5;
	pos = createVector(0,0);
	if(posPaths[index]==undefined || posPaths[index].collisionCount>topCollisionCount){
//		console.log("und");
	 	pos = createVector(p1.x,p1.y);
		dirx = (random(1)<0.5);
		done = false;
		collisionCount = 0;
		posPaths[index] = {pos: pos, dirx:dirx, path: [], done:done};
		posPaths[index].path.push(pos);
	}
	else{ 
		pos.set(posPaths[index].pos);
		dirx = posPaths[index].dirx;
		done = posPaths[index].done;
		collisionCount = posPaths[index].collisionCount;
	}
	var collided = false;
	var prev;
	var len = posPaths[index].path.length;
	if(len>1){
		prev = posPaths[index].path[len-2];
	}
	else{
		prev = posPaths[index].path[len-1];
	}
	if(p5.Vector.sub(p2,pos).mag()>stepsize){
		collided = false;
		dif = p5.Vector.sub(p2,pos);
		if(dif.mag()<stepsize){
			stepsize = 1;
		}
		difx = createVector(dif.x,0);
		difx.normalize();
		difx.mult(stepsize);
		dify = createVector(0,dif.y);
		dify.normalize();
		dify.mult(stepsize);
		do{
		if(collisionCount<2){
			if(dirx){
				pos.add(difx);
				if(isColliding(pos,collisionsize,p2) || p5.Vector.sub(pos,prev).mag()<stepsize ){
					pos.sub(difx);
					dirx = false;
					collided = true;
					collisionCount++;
				}
				else{
					collisionCount = 0;
				}
			}
			else{
				pos.add(dify);
				if(isColliding(pos,collisionsize,p2)  || p5.Vector.sub(pos,prev).mag()<stepsize){
					pos.sub(dify);
					dirx = true;
					collided = true;
					collisionCount++;
				}
				else{
					collisionCount = 0;
				}
			}
		}
		else{
			if(dirx){
				pos.sub(difx);
				if(isColliding(pos,stepsize,p2)){
					pos.add(difx);
					dirx = false;
					collided = true;
					collisionCount++;
				}
				else{
					collisionCount = 0;
				}
			}
			else{
				pos.sub(dify);
				if(isColliding(pos,stepsize,p2)){
					pos.add(dify);
					dirx = true;
					collided = true;
					collisionCount++;
				}
				else{
					collisionCount = 0;
				}
			}
		}
		}while(collisionCount>0 && collisionCount<topCollisionCount);
		if(random(1)<probchangedir){
			dirx = !dirx;
		}
		posPaths[index].path.push(pos);
	}
	posPaths[index].pos = pos;
	posPaths[index].dirx = dirx;
	posPaths[index].collisionCount = collisionCount;
	fill(255);
	drawThickPath(posPaths[index].path,th);
	/*
	 // Draw line
	beginShape();
	var v1,v2;
	for(var i=0; i<posPaths[index].path.length; i++){
		v1 = posPaths[index].path[i];
		vertex(v1.x,v1.y);
	}
	endShape();
	*/
}
function drawThickPath(path,th){
	var npath = path.length;
	beginShape();
	var v1,v2,v3,prev;
	var dif,dif1,dif2,addition;
	v1 = createVector(0,0);
	v2 = createVector(0,0);
	v3 = createVector(0,0);
	prev = createVector(0,0);
	addition = createVector(0,0);
	v1.set(path[0]);
	v2.set(path[1]);
	var dif = p5.Vector.sub(v2,v1);
	dif.normalize();
	dif.rotate(HALF_PI);
	dif.mult(th);
	v1.add(dif);
	// Previous to last
	vertex(v1.x,v1.y);
	// First vertex
	v1.sub(dif);
	v1.sub(dif);
	vertex(v1.x,v1.y);
	prev.set(v1);
	var heading;
	for(var i=1; i<npath-1; i++){
		v1.set( path[i-1]);
		v2.set(path[i]);
		v3.set(path[i+1]);
		dif1 = p5.Vector.sub(v2,v1);
		dif1.normalize();
		dif1.rotate(-HALF_PI);
		dif2 = p5.Vector.sub(v3,v2);
		dif2.normalize();
		dif2.rotate(-HALF_PI);
		dif = p5.Vector.add(dif2,dif1);
		dif.normalize();
		heading = dif.heading();
		dif.mult(th);
		v2.add(dif);
		vertex(v2.x,v2.y);
	}
	// End of the stroke
	v1.set(path[npath-2]);
	v2.set(path[npath-1]);
	dif = p5.Vector.sub(v2,v1);
	dif.normalize();
	dif.rotate(HALF_PI);
	dif.mult(th);
	v2.sub(dif);
	vertex(v2.x,v2.y);
	// Starting the other side
	v2.add(dif);
	v2.add(dif);
	vertex(v2.x,v2.y);
	// Everything in reverse
	for(var i=npath-2; i>0; i--){
		v1.set( path[i-1]);
		v2.set(path[i]);
		v3.set(path[i+1]);
		dif1 = p5.Vector.sub(v2,v1);
		dif1.normalize();
		dif1.rotate(HALF_PI);
		dif2 = p5.Vector.sub(v3,v2);
		dif2.normalize();
		dif2.rotate(HALF_PI);
		dif = p5.Vector.add(dif2,dif1);
		dif.normalize();
		heading = dif.heading();
		dif.mult(th);
		v2.add(dif);
		vertex(v2.x,v2.y);
	}
	v1.set(path[0]);
	v2.set(path[1]);
	dif = p5.Vector.sub(v2,v1);
	dif.normalize();
	dif.rotate(HALF_PI);
	dif.mult(th);
	v1.add(dif);
	// Previous to last
	vertex(v1.x,v1.y);
	endShape();
//	endShape(CLOSE);
}
function isColliding(pos,d,except){
	var eqth = 0.1;
	var v1;
	for(var i=0; i<N; i++){
		v1 = nodes[i].pos;
		if(p5.Vector.sub(v1,except).mag()>eqth && p5.Vector.sub(v1,pos).mag()<d){
			return true;
		}
	}
	for(var i in univ){
		v1 = univ[i].vec;
		if(p5.Vector.sub(v1,except).mag()>eqth && p5.Vector.sub(v1,pos).mag()<d){
			return true;
		}
	}
//	return false;
	return isInRect(pos.x,pos.y,freeRect.x+d,freeRect.y+d,freeRect.w-2*d,freeRect.h-2*d);
}
function isInRect(x1,y1,x,y,w,h){
	return x1>=x && x1<=x+w && y1>=y && y1<=y+h;
}
function isMouseWithin(x,y,w,h){
	return mouseX>=x && mouseX<=x+w && mouseY>=y && mouseY<=y+h;
}
function unfocusEverything(){
	unfocusWordsWithNodes();
	unfocusNodes();
	unfocusEdges();
}
function unfocusWordsWithNodes(){
	for(i in univ){
		univ[i].focusNode = false;
	}
}
function focusWordsWithNode(n){
	for(i in univ){
		if(n.words.includes(univ[i].word)){
			univ[i].focusNode = true;
		}
	}
}
function focusWithWord(w){
	unfocusEdges();
	unfocusNodes();
	focusEdgesWithWord(w);
	focusNodesWithWord(w);
}
function focusNodesWithWord(w){
	for(i in nodes){
		if(nodes[i].words.includes(w)){
			nodes[i].focus = true;
		}
	}
}
function unfocusNodes(){
	for(i in nodes){
		nodes[i].focus = false;
	}
}
function focusEdgesWithWord(w){
	for(i in edges){
		if(edges[i].words.includes(w)){
			edges[i].focus = true;
		}
	}
}
function unfocusEdges(){
	for(i in edges){
		edges[i].focus = false;
	}
}
function getEdgeLength(i,j){
	var e = getEdge(i,j);
	return e.words.length;
}
function getEdgeNormWeight(i,j){
	var e = getEdge(i,j);
	var l = e.words.length;
	return map(l,minLinks,maxLinks,0,1);
}
function getEdge(i,j){
	var e;
	// If i>j, swap values 
	if(i>j){
		var t = i;
		i = j;
		j = t;
	}
	for(d in edges){
		e = edges[d];
		if(e.key === i+","+j){
			return e;
		}
	}
}
function calculateMinAndMaxLinkWordsN(){
	var min = edges[0].words.length;
	var max = min;
	var l;
	for(i in edges){
		l = edges[i].words.length;
		if(l<min){
			min = l;
		}
		if(l>max){
			max = l;
		}
	}
	minLinks = min;
	maxLinks = max;
	console.log("Links N -  Max: "+max+" Min: "+min);
}
