var nlp = window.nlp_compromise;

function setup(){
	createCanvas(500,500);
	background(25);
//	createP(text);
	/*
    var t = nlp('dinosaur').nouns().toPlural();
	createP(t.normal());
	*/

	var t = nlp.text(text);
	var tags = t.tags();
	var terms = t.terms();
	var ns = t.nouns();

	var tag;
	var count = 0;

	var rowsize = 48;
	var squareSize = 10;
	var shapeScale = 0.65;
	var x = 0;
	var y = 0;

	noStroke();
	ellipseMode(CORNER);
	translate(squareSize,squareSize);

	for(var i=0; i<tags.length; i++){
		for(var j=0; j<tags[i].length; j++){
			tag = tags[i][j];

			count++;

			x = (count%rowsize)*squareSize;
			y = int(count/rowsize)*squareSize;

			createP(tags[i][j] + ' ' + x + ' '+y);
			if(tag == 'Noun'){
				ellipse(x,y,squareSize*shapeScale,squareSize*shapeScale);
				//rect(x,y,squareSize*shapeScale,squareSize*shapeScale);
			}
		}
	}
	createP(count);
	createP(terms.length);

}

function draw(){


}
