var texts = ['Baby','Fandom','Romein','Walker','Kauffmann','Despina','Wippy','Zurkow','Rozin','Lazarow','David','Clay','Depaz','VanEvery','Dorsainville','Dillon'];
var titles = ["Applying baby instincts in designing new technology",
		"Welcome to the fandom singularity",
		"Digital Body",
		"Abstraction, Distraction, Automation",
		"V-Aren't: Looking for meaning in immersive media",
		"Adjacencies",
		"Call me Adele",
		"The Temporary Expert",
		"Why Mirrors?",
		"Caveat Emptor",
		"Form within form, code within code",
		"Clay's article",
		"Expressive code",
		"As we may think",
		"Diversity and bias",
		"100 Days of Making"
];




var edges;

var nodesWords;

var universe;
var univ =[];

var nodes = [];

var N = texts.length;

var minLinks;
var maxLinks;

var t;

var colorFocus; 

function preload(){
	edges = loadJSON("data/links.json");
	nodesWords = loadJSON("data/nodeWords.json");
	universe = loadJSON("data/universe.json");
}

function setup(){
	createCanvas(800,650);

	background(255);

	colorFocus = color(255,185,0,230);

	for(i in edges){
		edges[i].focus = false;
	}

	for(i in universe){
		univ.push({word:universe[i],focusMouse:false,focusNode:false});
	}

	univ.sort( function(a,b){ 
		var wa = a.word;
		var wb = b.word;
		if(wa<wb){
			return -1;
		}
		if(wa>wb){
			return 1;
		}
		return 0;
		});
	// univ sort?

	var x,y;
	var r = 30; // radius of original arrangement
	var d=20; // diameter of node
	var node = [];
	var a = 0;
	var a2,r2;
	for(var i=0; i<N; i++){
//		a2 = a + random(-PI/8,PI/8);
		a2 = a + random(0,2*PI);
/*
		r2 = r + random(-1,1)*r*0.01;
		*/
		r2 = r;
		x = r2*cos(a2) + width/2; 
		y = r2*sin(a2) + height/2;
		node =[];
		node.pos = createVector(x,y);
		node.originalpos = createVector(x,y);
		node.index = i;
		node.name= texts[i];
		node.title = titles[i];
		node.focus = false;
		node.words = nodesWords[i];
		a+= TWO_PI/(N+1);
		nodes.push(node);
	}


	t = 0;

	calculateMinAndMaxLinkWordsN();

//	focusWordsWithNode(nodes[0]);




}

function draw(){

	// Diameter of node
	var d = 25;

	// Update positions
	var addition;
	var f = 0.02;
	var attraction;
	var repulsion;
	var n1,n2;

	var repfactor = 10000;
	var attfactor = 3;



	for(var i=0; i<N; i++){
		attraction = createVector(0,0);
		repulsion = createVector(0,0);
		n1 = nodes[i].pos;
		for(var j=0; j<N; j++){
			if(i!=j){
				n2 =  nodes[j].pos;
				var dif = p5.Vector.sub(n2,n1);
				var m = dif.mag();
				var rep = createVector(0,0);
				var w = getEdgeNormWeight(i,j);
				dif.normalize();

				rep.set(dif);
				// Repulsion is inversely proportional
				// To distance
				// And to weight?
//				rep.mult(-1/(m*m*(w+0.01)));
				rep.mult(-1/(m*m+w));
				rep.mult(repfactor);
				repulsion.add(rep);

			 	// Attraction is proportional to sq(weight)
				dif.mult(w*w);
				dif.mult(attfactor);
				attraction.add(dif);

			}
		}



		nodes[i].pos.add(attraction);
		nodes[i].pos.add(repulsion);

		// Add continuous movement
		var keepMoving = createVector(0,0);
		keepMoving.set(nodes[i].pos);
		keepMoving.sub(width/2,height/2);
		keepMoving.normalize();
		keepMoving.rotate(HALF_PI);
		keepMoving.mult(0.3);
		nodes[i].pos.add(keepMoving);


		// Contrain positions
		var margin  = d*4;
		var fr = 1.5;
		var fl = 1.2;
		var ft = 1.0;
		var fb = 1.0;
		nodes[i].pos.x = constrain(nodes[i].pos.x,margin*fr,width-margin*fl);
		nodes[i].pos.y = constrain(nodes[i].pos.y,margin*ft,height-margin*fb);

	}

	background(255);
	stroke(0);

	var v1,v2;
	var mid;
	var tSize = 11;


	textAlign(LEFT);
	// DRAW NODES AND EDGES
	textSize(tSize);
	for(var i=0; i<N; i++){
		v1 = nodes[i].pos;
		// Draw edges
		for(var j=i+1; j<N; j++){
			var edge = getEdge(i,j);
			v2 = nodes[j].pos;


			if(edge.focus){
				strokeWeight(20*getEdgeNormWeight(i,j));
				stroke(colorFocus,100);
			}
			else{
				strokeWeight(10*getEdgeNormWeight(i,j));
				stroke(100,80);
			}

			line(v1.x,v1.y,v2.x,v2.y);



			mid = p5.Vector.sub(v1,v2);
			mid = mid.mult(0.5+ 0.4*sin((i+1)*0.1*t+j/N));
			mid = mid.add(v2);
			strokeWeight(1);
//			ellipse(mid.x,mid.y,5,5);

			var h = 0;
			noStroke();
			textStyle(NORMAL);
			/*
			for(var w in edge.words){
				text(edge.words[w],mid.x+tSize,mid.y+h);
				h+=tSize;
			}
			*/
			// One Word
//				text(edge.words[0],mid.x+tSize,mid.y+h);

		}

		if(isMouseWithin(v1.x-d/2,v1.y-d/2,d,d)){
			unfocusEverything();
			focusWordsWithNode(nodes[i]);
			nodes[i].focus = true;
		}

		// Draw node
		if(nodes[i].focus){
		stroke(colorFocus,200);
		strokeWeight(4);
		}
		else{
		strokeWeight(1);
		stroke(100,200);
		}
		fill(255,240);
		ellipse(v1.x,v1.y,d,d);

	}

	// DRAW nodes text
	for(var i=0; i<N; i++){
		v1 = nodes[i].pos;
		noStroke();
		fill(0);
		textStyle(BOLD);
		if(nodes[i].focus){
		textSize(tSize*1.15);
		}
		else{
		textSize(tSize*1.05);
		}
		text(nodes[i].title,v1.x+d*0.5,v1.y,tSize*10,tSize*10);
	}

	// WORDS RECTANGLES

	var ypos = 10;
	var yinc = 11;
	var xwidth = textWidth("communication");
	var s;
	var xpos = 20;
	for(i in univ){
		s = univ[i].word;
		if(isMouseWithin(xpos,ypos-yinc,xwidth,yinc)){
			unfocusEverything();
			univ[i].focusMouse = true;
			focusWithWord(s);
		}
		else{
			univ[i].focusMouse = false;
		}

		if(univ[i].focusMouse || univ[i].focusNode){
			fill(0);
			rect(xpos,ypos-tSize,xwidth,yinc+2);
		}
		ypos+=yinc;
	}
	
	ypos=10;

	// WORDS
	for(i in univ){
		s = univ[i].word;
		if(univ[i].focusMouse || univ[i].focusNode){
			fill(colorFocus,255);
			textSize(tSize*1.05);
		}
		else{
			fill(0);
			textSize(tSize);
		}
		text(s,xpos,ypos);
		ypos+=yinc;
	}



	// Descriiption
	//
	fill(0);
	var description = "Relationships between most important nouns";

	var info = "[info]";

	// 
	var description2 = "The list in the left contains the three most important nouns of every article, and all the nouns that connect two articles. A noun connects two articles when it exists in both their lists of 42 most important nouns. The most important nouns of an article are defined with tf-idf: the most frequent nouns in an article when it is compared with the rest of the journal. The graph moves to find a spatial configuration that corresponds to the closeness between articles."
	// Tamaño de letra
	noStroke();
	textSize(tSize*1.3);
	textAlign(RIGHT);
	text(description,width,10);


	fill(100);
	textSize(tSize*1.1);
	textAlign(LEFT);
	var infox = width-tSize*3;
	var infoy = 25;

//	rect(infox,infoy-tSize*1.1,textWidth(info),tSize*1.5);
	text(info, infox, infoy); 
	if(isMouseWithin(infox,infoy-tSize*1.1,textWidth(info),tSize*1.5)){
	var tlen = 60;
	text(description2,width-tSize*tlen,tSize*3.0,tSize*tlen,tSize*10);
	}


	t+=0.001;



}

function isMouseWithin(x,y,w,h){
	return mouseX>=x && mouseX<=x+w && mouseY>=y && mouseY<=y+h;
}

function unfocusEverything(){
	unfocusWordsWithNodes();
	unfocusNodes();
	unfocusEdges();
}

function unfocusWordsWithNodes(){
	for(i in univ){
		univ[i].focusNode = false;
	}
}

function focusWordsWithNode(n){
	for(i in univ){
		if(n.words.includes(univ[i].word)){
			univ[i].focusNode = true;
		}
	}


}


function focusWithWord(w){
	unfocusEdges();
	unfocusNodes();
	focusEdgesWithWord(w);
	focusNodesWithWord(w);
}

function focusNodesWithWord(w){
	for(i in nodes){
		if(nodes[i].words.includes(w)){
			nodes[i].focus = true;
		}
	}
}

function unfocusNodes(){
	for(i in nodes){
		nodes[i].focus = false;
	}
}

function focusEdgesWithWord(w){
	for(i in edges){
		if(edges[i].words.includes(w)){
			edges[i].focus = true;
		}
	}
}

function unfocusEdges(){
	for(i in edges){
		edges[i].focus = false;
	}
}

function getEdgeNormWeight(i,j){
	var e = getEdge(i,j);
	var l = e.words.length;
	return map(l,minLinks,maxLinks,0,1);

}

function getEdge(i,j){
	var e;
	// If i>j, swap values 
	if(i>j){
		var t = i;
		i = j;
		j = t;
	}
	for(d in edges){
		e = edges[d];
		if(e.key === i+","+j){
			return e;
		}
	}

}

function calculateMinAndMaxLinkWordsN(){
	var min = edges[0].words.length;
	var max = min;

	var l;
	for(i in edges){
		l = edges[i].words.length;
		if(l<min){
			min = l;
		}

		if(l>max){
			max = l;
		}
	}

	minLinks = min;
	maxLinks = max;

	console.log("Links N -  Max: "+max+" Min: "+min);
}



